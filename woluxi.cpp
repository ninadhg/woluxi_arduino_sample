/*
 * MIT License
 *
 * Copyright (c) 2017 Woluxi

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "Arduino.h"
#include "woluxi.h"

Woluxi::Woluxi(int led, byte *vendor_id, byte hw_id, bool whitelabel, bool errno_only) {
  this->led = led;
  memcpy(this->vendor_id, vendor_id, 2);
  this->hw_id = hw_id;
  pinMode(led, OUTPUT);
  this->whitelabel = whitelabel;
  this->errno_only = errno_only;
}

void Woluxi::send_message(int msg_type, 
                 char *msg_id,
                 byte *ch,
                 int data_len){
  int i;
  this->woluxi_send_byte(PREAMBLE);
  if (this->errno_only) {
    this->woluxi_send_byte(ch[0]);
    Serial.println("finished sending errno only data");
    return;
  }
  this->woluxi_send_meta_data(msg_type,data_len);
  if (!this->whitelabel) {
    this->woluxi_send_vendor_id(this->vendor_id);
    this->send_hw_id(this->hw_id);
  }
  Serial.println("sending data");
  for(i = 0 ; i < data_len; i++){
    if(ch[i] == 0xf0) {
       this->woluxi_send_byte(ch[i]);
    }
    this->woluxi_send_byte(ch[i]);
  }
  Serial.println("finished data");
}

void Woluxi::woluxi_send_byte(char ch){
   int i;
   this->woluxi_send_bit(START_BIT);
   Serial.println(ch, HEX);
   for(i = 0 ; i <= 7; i++){
     this->woluxi_send_bit((ch<<i)&0x80);
   }
   this->woluxi_send_bit(STOP_BIT);
}

void Woluxi::woluxi_send_meta_data(int msg_type, byte len){
  char data = 0x00;
  len = len -1;
  if (len > 0x7 ) {
    Serial.println("too big a message");
    return;
  }
  switch(msg_type) {
    case ERRNO_DISPLAY_MODE:
       data = 0x00;
       break;
    case VENDOR_SPECIFIC_MODE:
       data = 0x10;
       break;
    case CUSTOM_MODE:
       data = 0x20;
       break;
    case ASCII_DISPLAY_MODE:
       data = 0x30;
       break;
    default :
      Serial.println("unknown message type");
      return;
  }
  data = data | len;
  this->woluxi_send_byte(data);
}

void Woluxi::woluxi_send_vendor_id(byte *vendor_id){
  this->woluxi_send_byte(this->vendor_id[0]);
  this->woluxi_send_byte(this->vendor_id[1]);
}

void Woluxi::send_hw_id(byte  hw_id){
  this->woluxi_send_byte(this->hw_id);
}

void Woluxi::woluxi_send_bit(int i){
  digitalWrite(this->led, i);
  delay(DELAY);
}
