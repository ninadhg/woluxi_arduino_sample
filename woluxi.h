/*
 * MIT License
 *
 * Copyright (c) 2017 Woluxi

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
 
#ifndef Woluxi_h
#define Woluxi_h

 

const byte PREAMBLE = 0xf0;
const int DELAY = 250;
const int PROTOCOL_VERSION = 0x00;

const int ERRNO_DISPLAY_MODE = 0;
const int VENDOR_SPECIFIC_MODE = 1;
const int CUSTOM_MODE = 2;
const int ASCII_DISPLAY_MODE = 3;

class Woluxi{
  public:
    Woluxi(int led, byte *vendor_id, byte hw_id, bool whitelabel, bool errno_only);
    void send_message(int msg_type, 
                 char *msg_id,
                 byte *ch,
                 int data_len);
  private :
    int _pin;
    int led;
    byte vendor_id[2];
    byte hw_id;
    bool whitelabel;
    bool errno_only;
    
    const int START_BIT = 1;
    const int STOP_BIT = 0;

    void woluxi_send_bit(int i);
    void woluxi_send_byte(char ch);
    void woluxi_send_meta_data(int msg_type, byte len);
    void woluxi_send_vendor_id(byte *vendor_id);
    void send_hw_id(byte  hw_id);
};
#endif
